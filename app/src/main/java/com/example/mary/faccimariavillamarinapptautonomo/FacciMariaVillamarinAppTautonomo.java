package com.example.mary.faccimariavillamarinapptautonomo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class FacciMariaVillamarinAppTautonomo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facci_maria_villamarin_app_tautonomo);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("onStop", "Maria Fernanda Villamarin Cevallos");
    }
}
